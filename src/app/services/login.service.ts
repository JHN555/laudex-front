import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { api } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  /*Este servicio se encarga de realizar las peticiones hacia el back para la parte de usuarios (login, logout) */
  constructor(private http:HttpClient) { }


  //Se envian los datos de las credenciales al backend par su validacion
    postLogin(username:string, password:string) {
      let params = new HttpParams()
      .set('username', username)
      .set('password', password);
    return this.http.post<any>(api+'/api/login', params).pipe(tap (res => {
      this.saveToken(res.token); 
      this.saveMensaje(res.mensaje);
    }))

  }

  //Se guarda el token en local storage
  private saveToken(token: string): void {
    localStorage.setItem(
      'token',
      token
    );
  }

  //Se guarda el mensaje de bienvenida obtenido desde el back
  private saveMensaje(mensaje: string): void {
    localStorage.setItem(
      'mensaje',
      mensaje
    );
  }

  //Metodo para eliminar el token del local storage
  logout(): void {
    localStorage.removeItem(
      'token'
    );

    localStorage.removeItem(
      'mensaje'
    );

  }
}
