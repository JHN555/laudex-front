import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlumnoInterface } from '../interfaces/alumno.interface';
import { api } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlumnoService {

  /* Este servicio se encarga de realizar las peticiones al backend para la parte de alumnos */

  constructor(private http: HttpClient) { }

  // Se obtiene el token jwt desde el local storage
  token: any = localStorage.getItem(
    'token'
  );

  // Se establecen las cabeceras que seran enviadas al back, con el token jwt
  headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.token)

  getShowAlumno(idAlumno: number) {



    return this.http
      .get<AlumnoInterface>(api + '/api/alumnos/show/' + idAlumno, {
        headers: this.headers,
      });
  }


  getAlumnos() {
    return this.http
      .get<AlumnoInterface[]>(api + '/api/alumnos', {
        headers: this.headers,
      })
  }

  postAlumno(alumno: AlumnoInterface) {
    return this.http.post<any>(api + '/api/alumnos', alumno, {
      headers: this.headers,
    })
  }
}
