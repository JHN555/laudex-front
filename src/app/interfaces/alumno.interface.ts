export interface AlumnoInterface {
    id: number;
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    fechaNacimiento: string;
    sexo: string;
    gradoEstudiosActual: string;
    email: string;
    telefono: string;
}


