import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor() { }

  //Se carga mensaje de bienvendida
  mensaje:any = localStorage.getItem(
    'mensaje'
  );

  ngOnInit(): void {
    
  }
}
