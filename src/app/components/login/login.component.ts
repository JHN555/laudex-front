import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // Se inyecta servicio de login y router para cambiar de ruta
  constructor(private loginService: LoginService, private router:Router) { }

  username:string = "";
  password:string = "";
  mensaje:string = "";

  ngOnInit(): void {
  }

  //Metodo para enviar al back las credenciales del usuario
  enviar() {
    console.log("alumno", this.username);
    this.loginService.postLogin(this.username, this.password).subscribe(res => {
      console.log(res)

      //Se redirige a la ruta principal 
      this.router.navigateByUrl("/")

    }, err => {
      //Se muestra mensaje de error en caso de crendenciales incorrectas
      this.mensaje = "Credenciales incorrectas";
    })
  }

}
