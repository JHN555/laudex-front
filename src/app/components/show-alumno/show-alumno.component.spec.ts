import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAlumnoComponent } from './show-alumno.component';

describe('ShowAlumnoComponent', () => {
  let component: ShowAlumnoComponent;
  let fixture: ComponentFixture<ShowAlumnoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowAlumnoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAlumnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
