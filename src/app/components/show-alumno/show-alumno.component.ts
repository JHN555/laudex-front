import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlumnoInterface } from 'src/app/interfaces/alumno.interface';
import { AlumnoService } from 'src/app/services/alumno.service';

@Component({
  selector: 'app-show-alumno',
  templateUrl: './show-alumno.component.html',
  styleUrls: ['./show-alumno.component.css']
})
export class ShowAlumnoComponent implements OnInit {

  //Se inyecta servicio de alumno y route para conocer el valor del id del alumno
  constructor(private alumnoService:AlumnoService, private route:ActivatedRoute) { }

  alumno :AlumnoInterface = {} as AlumnoInterface

  ngOnInit(): void {
    //Se obtiene el valor id del la url para el alumno
    const id = Number(this.route.snapshot.paramMap.get('id'));

    //Se realiza peticion al back para obtener datos del alumno
    this.alumnoService.getShowAlumno(id).subscribe(res => {
      this.alumno = res;
      console.log(res);
    })
  }

}
