import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  //Se inyectan servicio de login y router para cambiar de ruta
  constructor(private loginService:LoginService, private router:Router) { }

  mensaje:any = localStorage.getItem(
    'mensaje'
  );

  ngOnInit(): void {
  
  }
//Se ejecuta funcion para cerrar sesion y se redirige a la ruta de login 
  logout(){
    this.loginService.logout();
    this.router.navigateByUrl("/login")
  }
}
