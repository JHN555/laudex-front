import { Component, OnInit } from '@angular/core';
import { AlumnoInterface } from 'src/app/interfaces/alumno.interface';
import { AlumnoService } from 'src/app/services/alumno.service';

@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.component.html',
  styleUrls: ['./alumnos.component.css']
})
export class AlumnosComponent implements OnInit {

  constructor(private alumnoService:AlumnoService ) { }

  // Se inicializa el objeto
  alumnosList: AlumnoInterface [] = []

  // Se obtiene la lista de alumnos
  ngOnInit(): void {
    this.alumnoService.getAlumnos().subscribe(res => {
      this.alumnosList = res
      console.log(res);
    })
  }

}
