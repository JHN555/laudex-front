import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveAlumnoComponent } from './save-alumno.component';

describe('SaveAlumnoComponent', () => {
  let component: SaveAlumnoComponent;
  let fixture: ComponentFixture<SaveAlumnoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveAlumnoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveAlumnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
