import { Component, OnInit } from '@angular/core';
import { AlumnoInterface } from 'src/app/interfaces/alumno.interface';
import { AlumnoService } from 'src/app/services/alumno.service';

@Component({
  selector: 'app-save-alumno',
  templateUrl: './save-alumno.component.html',
  styleUrls: ['./save-alumno.component.css']
})
export class SaveAlumnoComponent implements OnInit {

  //Se inyecto servicio de alumno
  constructor(private alumnoService: AlumnoService) { }

  ngOnInit(): void {
  }

  saveAlumno: AlumnoInterface = {} as AlumnoInterface
  mensaje: string = "";

  enviar() {

    // Se realizan las validaciones del formulario
    if (!this.saveAlumno.nombre) {
      alert("Ingrese campo nombre");
      return
    }

    if (!this.saveAlumno.apellidoPaterno) {
      alert("Ingrese campo paterno");
      return
    }
    if (!this.saveAlumno.apellidoMaterno) {
      alert("Ingrese campo materno");
      return
    }
    if (!this.saveAlumno.fechaNacimiento) {
      alert("Ingrese campo fecha nacimiento");
      return
    }
    if (!this.saveAlumno.sexo) {
      alert("Ingrese campo sexo");
      return
    }

    if (!this.saveAlumno.gradoEstudiosActual) {
      alert("Ingrese campo grado de estudios");
      return
    }

    if (!this.saveAlumno.email) {
      alert("Ingrese campo email");
      return
    }
    else {
      let validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      if (!this.saveAlumno.email.match(validRegex)) {
        alert("Email no valido");
        return;
      }
    }

    if (!this.saveAlumno.telefono) {
      alert("Ingrese campo telefono");
      return
    } else {
      let validRegex = /^[0-9]{10}$/;
      if (!this.saveAlumno.telefono.match(validRegex)) {
        alert("Telefono no valido");
        return;
      }
    }

    console.log("alumno", this.saveAlumno)

    //Se envia peticion al backend  con los datos del formulario
    this.alumnoService
      .postAlumno(this.saveAlumno)
      .subscribe(res => {
        console.log(res)
        this.mensaje = "Se registro correctamente el alumno";
      }, erro => {
        //En caso de error muestra un mensaje
        this.mensaje = "No se pudo registrar el alumno";
      });
  }

}
