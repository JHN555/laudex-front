import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlumnosComponent } from './components/alumnos/alumnos.component';
import { IndexComponent } from './components/index/index.component';
import { LoginComponent } from './components/login/login.component';
import { SaveAlumnoComponent } from './components/save-alumno/save-alumno.component';
import { ShowAlumnoComponent } from './components/show-alumno/show-alumno.component';

// Se definen la rutas 
const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "alumnos", component: AlumnosComponent },
  { path: "save-alumno", component: SaveAlumnoComponent },
  { path: "show-alumno/:id", component: ShowAlumnoComponent },
  { path: "", component: IndexComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
